# DigitalOcean Tutorial Prerequisite Playbooks

This set of playbooks dynamically creates, configures and destroys DigitalOcean Droplets using the DO API v2.

## Prerequisites:

### A clean Ubuntu 16.04 server
### Ansible 2.2+


    sudo apt-get install software-properties-common
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get install ansible


### pip: 

  ```apt-get install python-pip```

### dopy: 

  ```pip install dopy```

## Overview

[TODO: Describe assumptions and use case]

 - The `base_setup.yml` playbook will look for ssh keys in the account of the user running the command,
create them if they're missing, add them to the DO account, and transfer the public key to the servers it creates so that Ansible can interact with those new servers. 

This is a little fragile because if you already have a key by the same name, the playbook won't update it. You'll either need to rename or remove the key on your account, or update its name in the playbooks.

 - `create_droplet.yml` is included by `base_setup.yml` so it runs automatically. Neither of these files should require updating at this time. To get started, begin with the steps below.

## Step 1 (Optional) — Disabling StrictHostKeyChecking

Edit `/etc/ssh/ssh_config` and set `StrictHostKeyChecking no`
You can manually accept host keys, but you may have to run the `create_droplets.yml` playbook multiple times due to timeouts.


## Step 2 — Setting the API token (One-Time Only)

While logged into your DigitalOcean account, visit the [API [page](https://cloud.digitalocean.com/settings/api/tokens/new) and create an API token.

Copy it into a file in the project root called `do_token.txt`. This filename is already included in a `.gitignore` file and configured in the playbooks. If you change it, you'll need to update it in playbooks and the `gitignore file`.


## Step 3 — Configuring Droplets for Creation

Droplets are configured in the `vars.yml` file. In order for the Droplets to be created, the names you 
choose for them must be unique among Droplets in your account. Otherwise, the `create_droplets.yml` playbook will see that the name exists and move on without applying any additional changes.

## Step 4 — Running Playbooks
The `base_setup.yml` playbook will pass Droplet details along to the `tutorial-ubuntu-16_04.yml` playbook dynamically, so generally, you'll run both of these at the same time as follows:

```
ansible-playbook base_setup.yml  tutorial-ubuntu-16_04.yml -e "servers=servers-ubuntu-16.04.yml"
```
Note that there is often a delay in reaching the new hosts after `create_droplets.yml` runs, so you might want to run it alone, then wait a few minutes, then run the two together.

## Step 5 — Destroying Droplets
The `destroy_droplets.yml` playbook will use the names specified in the `vars.yml` file to delete Droplets. WARNING: There is no ID checking, and the playbook will delete all Droplets with matching names.
